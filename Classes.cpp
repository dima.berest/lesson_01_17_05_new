/*
1. ������� ����� �� ������ �������, �������� ��� ������ private. 
������� public ����� ��� ������ ���� ������. ��������������.

2. ��������� ����� Vector public �������, 
������� ����� ���������� ����� (������) �������. ��������������.
*/

#include <iostream>
#include <cmath>

class Date
{
public:
	void printDate()
	{
		std::cout << "Date: " << day << '.' << month << '.' << year << "\n\n";
	}

private:
	int day = 24;
	int month = 2;
	int year = 2022;
};

class Vector
{
public:
	Vector(double _x = 5.5, double _y = 7.2, double _z = 13.23) : x(_x), y(_y), z(_z)
	{}
	void show()
	{
		std::cout << "Vector coords: " << x << ' ' << y << ' ' << z << '\n';
	}
	double getVectorLength()
	{
		return std::sqrt((x * x) + (y * y) + (z * z));
	}

private:
	double x = 0;
	double y = 0;
	double z = 0;
};


int main()
{
	// 1.:
	Date ukraineWarDate;
	ukraineWarDate.printDate();

	// 2.:
	Vector v;
	v.show();
	std::cout << "Vector length: " << v.getVectorLength() << "\n\n";
}